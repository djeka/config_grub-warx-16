
	 __      __             ____  ___         ____  ________
	/  \    /  \_____ ______\   \/  /        /_   |/  _____/
	\   \/\/   /\__  \\_  __ \     /   ______ |   /   __  \
	 \        /  / __ \|  | \/     \  /_____/ |   \  |__\  \
	  \__/\  /  (____  /__| /___/\  \         |___|\_____  /
	       \/        \/           \_/                    \/


**Config Grub/WarX-16**
*version: pré-alpha_20180917115538*


# Créer un iso minimal de la Config Grub/WarX-16
## commande grub-mkrescue:
> DEBIAN = paquet grub-common


```sh
git clone https://gitlab.com/WarX-16/Config/config_grub-warx-16.git
grub-mkrescue -o Config_Grub-WarX-16.iso config_grub-warx-16/
```

# Utiliser la Config Grub/WarX-16 sur un système installé
## commande update-grub:
> DEBIAN = paquet grub-common

`! EXPERIMENTAL !`
```sh
git clone https://gitlab.com/WarX-16/Config/config_grub-warx-16.git
sudo cp -vrb config_grub-warx-16/{boot,etc}/ /
sudo chmod +x /etc/grub.d/00_config_grub-warx-16
sudo update-grub
```





Copyright (C) 2018 WarX-16
 <contact@warx-16.org>.
* https://www.warx-16.org
* https://blog.warx-16.org
