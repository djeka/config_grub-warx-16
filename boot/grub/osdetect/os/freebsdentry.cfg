# Config Grub/WarX-16 - freebsdentry.cfg
# Copyright (C) 2018 WarX-16 <contact@warx-16.org>.
# https://www.warx-16.org
#
# Config Grub/WarX-16 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Config Grub/WarX-16 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Config Grub/WarX-16.  If not, see <https://www.gnu.org/licenses/>.
#
# Certain part of Config Grub/WarX-16 was inspired by Super Grub Disk
# ( Adrian Gibanel Lopez - Jordan Uggla - https://www.supergrubdisk.org/ )


#~ set part="${part}"
set root="${part}"
#~ set tscheme="${tscheme}"
#~ set fstype="${fstype}"
#~ set uuid="${uuid}"
set os_name="${os_find}"
#~ set os_ico="${os_ico}"
set ls_item="${ls_item}"

if regexp ';;' "${ls_item}"; then
	tr -s ls_item ';;' '  ' "${ls_item}"
fi

for item in ${ls_item}; do
	if [ ! -e "${item}" ]; then
		set out=true
	fi
done

if [ "${out}" != 'true' ]; then

	if [ "${fstype}" = ufs1 -o "${fstype}" = ufs2 ]; then

		submenu --class="${os_ico}" "(${part}:${tscheme}:${fstype}) [FreeBSD (ufs-variant)]" \
				"${part}" "${tscheme}" "${fstype}" "${uuid}" \
				"${os_name}" "${os_ico}" "${ls_item}" {
		set part="$2"
		set tscheme="$3"
		set fstype="$4"
		set uuid="$5"
		set os_name="$6"
		set os_ico="$7"
		set ls_item="$8"

		retour "${checkpoint}"

		menuentry --class="${os_ico}" "(${part}:${tscheme}:${fstype}) [FreeBSD (ufs-variant)]" "${part}" "${uuid}" {
		set root="$2"
		set uuid="$3"

		kfreebsd /boot/kernel/kernel
		set kFreeBSD.acpi_load=YES
		set kFreeBSD.hint.acpi.0.disabled=0
		set kFreeBSD.vfs.root.mountfrom=ufs:ufsid/${uuid}
		kfreebsd_loadenv /boot/device.hints
		}

		menuentry --class="${os_ico}" "(${part}:${tscheme}:${fstype}) [FreeBSD(ufs-variant)] ("$"single-user"")" "${part}" "${uuid}" {
		set root="$2"
		set uuid="$3"

		kfreebsd /boot/kernel/kernel -s
		set kFreeBSD.acpi_load=YES
		set kFreeBSD.hint.acpi.0.disabled=0
		set kFreeBSD.vfs.root.mountfrom=ufs:ufsid/${uuid}
		kfreebsd_loadenv /boot/device.hints
		}

		menuentry --class="${os_ico}" "(${part}:${tscheme}:${fstype}) [FreeBSD(ufs-variant)] ("$"verbose"")" "${part}" "${uuid}" {
		set root="$2"
		set uuid="$3"

		kfreebsd /boot/kernel/kernel -v
		set kFreeBSD.acpi_load=YES
		set kFreeBSD.hint.acpi.0.disabled=0
		set kFreeBSD.vfs.root.mountfrom=ufs:ufsid/${uuid}
		kfreebsd_loadenv /boot/device.hints
		}

		menuentry --class="${os_ico}" "(${part}:${tscheme}:${fstype}) [FreeBSD(ufs-variant)] ("$"no ACPI"")" "${part}" "${uuid}" {
		set root="$2"
		set uuid="$3"

		kfreebsd /boot/kernel/kernel -v
		unset kFreeBSD.acpi_load
		set kFreeBSD.hint.acpi.0.disabled=1
		set kFreeBSD.loader.acpi_disabled_by_user=1
		set kFreeBSD.vfs.root.mountfrom=ufs:ufsid/${uuid}
		kfreebsd_loadenv /boot/device.hints
		}

		menuentry --class="${os_ico}" "(${part}:${tscheme}:${fstype}) [FreeBSD(ufs-variant)] ("$"safe mode"")" "${part}" "${uuid}" {
		set root="$2"
		set uuid="$3"

		kfreebsd /boot/kernel/kernel -v
		unset kFreeBSD.acpi_load
		set kFreeBSD.hint.acpi.0.disabled=1
		set kFreeBSD.loader.acpi_disabled_by_user=1
		set kFreeBSD.hint.apic.0.disabled=1
		set kFreeBSD.hw.ata.ata_dma=0
		set kFreeBSD.hw.ata.atapi_dma=0
		set kFreeBSD.hw.ata.wc=0
		set kFreeBSD.hw.eisa_slots=0
		set kFreeBSD.hint.kbdmux.0.disabled=1
		set kFreeBSD.vfs.root.mountfrom=ufs:ufsid/${uuid}
		kfreebsd_loadenv /boot/device.hints
		}

		menuentry --class="${os_ico}" "(${part}:${tscheme}:${fstype}) [FreeBSD(ufs-variant)] ("$"Default boot loader"")" "${part}" {
		set root="$2"

		kfreebsd /boot/loader
		}

		}

	elif [ "${fstype}" = zfs ]; then

		submenu --class="${os_ico}" "(${part}:${tscheme}:${fstype}) [FreeBSD (zfs-variant)]" \
				"${part}" "${tscheme}" "${fstype}" "${uuid}" \
				"${os_name}" "${os_ico}" "${ls_item}" {
		set part="$2"
		set tscheme="$3"
		set fstype="$4"
		set uuid="$5"
		set os_name="$6"
		set os_ico="$7"
		set ls_item="$8"

		retour "${checkpoint}"

		menuentry --class="${os_ico}" "(${part}:${tscheme}:${fstype}) [FreeBSD (zfs-variant)]" "${part}" {
		set root="$2"

		kfreebsd /@/boot/kernel/kernel
		set kFreeBSD.acpi_load=YES
		set kFreeBSD.hint.acpi.0.disabled=0
		kfreebsd_module_elf /@/boot/kernel/opensolaris.ko
		kfreebsd_module_elf /@/boot/kernel/zfs.ko
		kfreebsd_module /@/boot/zfs/zpool.cache type=/boot/zfs/zpool.cache
		probe -l -s name "${root}"
		set kFreeBSD.vfs.root.mountfrom=zfs:$name
		kfreebsd_loadenv /@/boot/device.hints
	}

	menuentry --class="${os_ico}" "(${part}:${tscheme}:${fstype}) [FreeBSD (zfs-variant)] ("$"single-user"")" "${part}" {
		set root="$2"

		kfreebsd /@/boot/kernel/kernel -s
		set kFreeBSD.acpi_load=YES
		set kFreeBSD.hint.acpi.0.disabled=0
		kfreebsd_module_elf /@/boot/kernel/opensolaris.ko
		kfreebsd_module_elf /@/boot/kernel/zfs.ko
		kfreebsd_module /@/boot/zfs/zpool.cache type=/boot/zfs/zpool.cache
		probe -l -s name "${root}"
		set kFreeBSD.vfs.root.mountfrom=zfs:$name
		kfreebsd_loadenv /@/boot/device.hints
	}

	menuentry --class="${os_ico}" "(${part}:${tscheme}:${fstype}) [FreeBSD (zfs-variant)] ("$"verbose"")" "${part}" {
		set root="$2"

		kfreebsd /@/boot/kernel/kernel -v
		set kFreeBSD.acpi_load=YES
		set kFreeBSD.hint.acpi.0.disabled=0
		kfreebsd_module_elf /@/boot/kernel/opensolaris.ko
		kfreebsd_module_elf /@/boot/kernel/zfs.ko
		kfreebsd_module /@/boot/zfs/zpool.cache type=/boot/zfs/zpool.cache
		probe -l -s name "${root}"
		set kFreeBSD.vfs.root.mountfrom=zfs:$name
		kfreebsd_loadenv /@/boot/device.hints
	}

	menuentry --class="${os_ico}" "(${part}:${tscheme}:${fstype}) [FreeBSD (zfs-variant)] ("$"no ACPI"")" "${part}" {
		set root="$2"

		kfreebsd /@/boot/kernel/kernel -v
		unset kFreeBSD.acpi_load
		set kFreeBSD.hint.acpi.0.disabled=1
		set kFreeBSD.loader.acpi_disabled_by_user=1
		kfreebsd_module_elf /@/boot/kernel/opensolaris.ko
		kfreebsd_module_elf /@/boot/kernel/zfs.ko
		kfreebsd_module /@/boot/zfs/zpool.cache type=/boot/zfs/zpool.cache
		probe -l -s name "${root}"
		set kFreeBSD.vfs.root.mountfrom=zfs:$name
		kfreebsd_loadenv /@/boot/device.hints
	}

	menuentry --class="${os_ico}" "(${part}:${tscheme}:${fstype}) [FreeBSD (zfs-variant)] ("$"safe mode"")" "${part}" {
		set root="$2"

		kfreebsd /@/boot/kernel/kernel -v
		unset kFreeBSD.acpi_load
		set kFreeBSD.hint.acpi.0.disabled=1
		set kFreeBSD.loader.acpi_disabled_by_user=1
		set kFreeBSD.hint.apic.0.disabled=1
		set kFreeBSD.hw.ata.ata_dma=0
		set kFreeBSD.hw.ata.atapi_dma=0
		set kFreeBSD.hw.ata.wc=0
		set kFreeBSD.hw.eisa_slots=0
		set kFreeBSD.hint.kbdmux.0.disabled=1
		kfreebsd_module_elf /@/boot/kernel/opensolaris.ko
		kfreebsd_module_elf /@/boot/kernel/zfs.ko
		kfreebsd_module /@/boot/zfs/zpool.cache type=/boot/zfs/zpool.cache
		probe -l -s name "${root}"
		set kFreeBSD.vfs.root.mountfrom=zfs:$name
		kfreebsd_loadenv /@/boot/device.hints
	}

	menuentry --class="${os_ico}" "(${part}:${tscheme}:${fstype}) [FreeBSD (zfs-variant)] ("$"Default boot loader"")" "${part}" "${uuid}" {
		set root="$2"

		kfreebsd /@/boot/loader
	}

}

	fi

fi
